/// https://doc.rust-lang.org/reference/procedural-macros.html#function-like-procedural-macros

extern crate proc_macro;
use proc_macro::TokenStream;

// mod token_matcher;
// use token_matcher::{token_matcher,test_token_matcher};

// const RUN_TESTS: bool = false;

#[proc_macro_attribute]
pub fn make_answer(
    _attributes_raw: TokenStream,
    _module_raw: TokenStream,
) -> TokenStream {
    // if RUN_TESTS {
    //     test_token_matcher()
    // }

    // for attribute in attributes_raw {
    //     match attribute {
    //         another => println!("Something else: {:#?}", another),
    //     }
    // }
    // for token in module_raw {
    //     println!("{}", token_matcher(&token))
    // }
    "fn answer() -> u32 { 442 }".parse().unwrap()
}
/*
#[cfg(test)]
mod tests {

    use super::make_answer;

    #[make_answer { 123, ok="abc"}]
    mod foo {
        struct Bar {}
    }    

    #[test]
    /// Tests make_answer().
    fn test_make_answer() {
        assert_eq!(443, answer());
    }
}
*/