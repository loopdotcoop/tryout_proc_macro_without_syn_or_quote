use proc_macro::{Ident,Span,TokenTree};

pub fn token_matcher(token: &TokenTree) -> String {
    match token {
        TokenTree::Ident(ident) => format!("Ident: {:#?}", ident),
        TokenTree::Group(group) => format!("Group: {:#?}", group),
        another => format!("Something else: {:#?}", another),
    }
}

pub fn test_token_matcher() {
    assert_eq!(
        "Ident: Ident {\n    ident: \"hullo\",\n    span: #4 bytes(31..62),\n}",
        token_matcher(
            &TokenTree::Ident(
                Ident::new(
                    "hullo",
                    Span::call_site(),
                )
            )
        )
    );
    println!("\x1b[42m test_token_matcher() completed ok √ \x1b[0m")
}

/*
#[cfg(test)]
mod tests {

    use super::test_token_matcher;

    #[test]
    /// Tests token_matcher().
    fn run_test_token_matcher() {
        test_token_matcher();
    }
}
*/

/*
#[cfg(test)]
mod tests {
    use proc_macro::{Ident,Span,TokenTree};
    use super::token_matcher;

    #[test]
    fn test_parse_attributes() {
        assert_eq!(
            "okok",
            token_matcher(
                &TokenTree::Ident(
                    Ident::new(
                        "hullo",
                        Span::call_site(),
                    )
                )
            )
        );
    }
}
*/