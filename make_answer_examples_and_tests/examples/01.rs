use make_answer::make_answer;

#[make_answer {}]
mod foo {
    struct Bar {}
}

fn main() {
    println!("ok")
}

//     #[cfg(test)]
// mod tests {
//     super::answer;

//     #[test]
//     /// Tests make_answer().
//     fn test_make_answer() {
//         assert_eq!(443, answer());
//     }
// }
