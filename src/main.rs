use make_answer::make_answer;

#[make_answer { 123, ok="abc"}]
mod foo {
    struct Bar {}
}

/*
TokenStream [
    Ident {
        ident: "mod",
        span: #0 bytes(76..79),
    },
    Ident {
        ident: "foo",
        span: #0 bytes(80..83),
    },
    Group {
        delimiter: Brace,
        stream: TokenStream [
            Ident {
                ident: "struct",
                span: #0 bytes(90..96),
            },
            Ident {
                ident: "Bar",
                span: #0 bytes(97..100),
            },
            Group {
                delimiter: Brace,
                stream: TokenStream [],
                span: #0 bytes(101..103),
            },
        ],
        span: #0 bytes(84..105),
    },
]
*/

fn main() {
    println!("{}", answer());
}
